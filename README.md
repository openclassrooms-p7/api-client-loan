# api-client-loan

[[_TOC_]]

## Overview
This project is made as a part of my Master's in Data science with Openclassrooms and contains the code for making an inference with a pretrained LightGBM model
to predict if a client can repay his loan back or not.

## Files
- This project contains the folder `api_p7` that has the API implemented with `FastAPI` and `univorn`.
- The folder `mlruns` contains the model trained by `MLFlow`.
- The `.gitlab-ci.yml` and `Dockerfile` contribute to generating a docker image containing the API which is uploaded to dockerhub on a push to the `main` branch.

## Hosting
The docker image is pulled from the dockerhub and is hosted on [render](https://www.render.com).
The link for the API will be included in the project submission, but for security purposes, it will not be shown in this repository.

## Final result
The API is used by a frontend that searches a given client by their ID and predicts whether they can pay their loan back or not.
The dashboard can be found here: [Dashboard](https://client-loan-dashboard-p7.streamlit.app/)
The code for the dashboard can be found here: [Code](https://github.com/sriram-vadlamani/client-loan-dashboard)
