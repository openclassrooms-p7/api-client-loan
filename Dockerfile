FROM python:3.10-slim as builder
WORKDIR /app
COPY api_p7 /app/api_p7
COPY mlruns /app/mlruns
COPY requirements.txt /app/requirements.txt
RUN apt-get update && apt-get install -y libgomp1
RUN pip install -r requirements.txt

CMD ["python", "api_p7/inference.py"]
